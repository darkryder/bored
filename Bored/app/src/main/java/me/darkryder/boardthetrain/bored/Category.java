package me.darkryder.boardthetrain.bored;

/**
 * Created by darkryder on 4/2/16.
 */
public enum Category {
    POLITICS,
    SPORTS,
    ENGINEERING
}
