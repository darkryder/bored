package me.darkryder.boardthetrain.bored;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.trnql.smart.base.SmartFragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends SmartFragment {

    public MainActivityFragment() {
    }

    private String name = "John Doe";
    private String trainName = "";
    private String coachName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        final Circle circle = (Circle) view.findViewById(R.id.circle);

        EditText personNameEditText = (EditText) view.findViewById(R.id.person_name);
        if(personNameEditText != null)
        {
            personNameEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {name = editable.toString();}
            });
        }

        final EditText coachNameEditText = (EditText) view.findViewById(R.id.coach_name);
        if(coachNameEditText != null)
        {
            coachNameEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    coachName = editable.toString();
                }
            });
        }

        View search_button = view.findViewById(R.id.search_button);
        final Context context = getActivity();
        if (search_button != null)
        {
            search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (trainName ==  null || trainName.length() == 0)
                    {
                        Toast.makeText(context, "Enter train number", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (coachName ==  null || coachName.length() == 0)
                    {
                        Toast.makeText(context, "Enter coach/seat details", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (name ==  null || name.length() == 0)
                    {
                        Toast.makeText(context, "Enter your name", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // creating data payload
                    long epoch_time_created = System.currentTimeMillis();
                    Category category = Category.ENGINEERING;
                    StringBuilder sb = new StringBuilder();
                    sb.append("{\"category\":");
                    sb.append(category.ordinal());
                    sb.append(",\"when\": ");
                    sb.append(epoch_time_created);
                    sb.append(",\"name\":");
                    sb.append("\"" + name + "\"");
                    sb.append(",\"coach\":");
                    sb.append("\"" + coachName +"\"");
                    sb.append("}");

                    String data = sb.toString();
                    getPeopleManager().setProductName(trainName);
                    getPeopleManager().setDataPayload(data);

                    Log.d("PeopleManager", "Setting " + data);

                    CircleAngleAnimation animation = new CircleAngleAnimation(circle, 0, 360);
                    animation.setDuration(2000);
                    circle.startAnimation(animation);
                }
            });
        }

        return view;
    }
}
