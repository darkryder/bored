package me.darkryder.boardthetrain.bored;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.trnql.smart.base.SmartCompatActivity;
import com.trnql.smart.people.PersonEntry;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends SmartCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String user_token = UUID.randomUUID().toString();

        getAppData().setApiKey("1ba51dda-c9fc-4061-bbd6-b8a06246b3d4");
        getAppData().startAllServices();
        getPeopleManager().setSearchRadius(3 * 1000);
        getPeopleManager().setUserToken(user_token);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void smartPeopleChange(List<PersonEntry> people) {
        super.smartPeopleChange(people);
        Log.d("SmartPeople", "SmartPeople triggered");
        for(PersonEntry personEntry: filterPeopleByDate(people))
        {
            Log.d("SmartPeopleInfo", people.toString());
        }
    }

    public static ArrayList<PersonEntry> filterPeopleByDate(List<PersonEntry> people)
    {
        long now = System.currentTimeMillis();
        long difference = 1000 * 60 * 60; // 1 hour
        ArrayList<PersonEntry> filtered = new ArrayList<>();
        for(PersonEntry person: people)
        {
            String dataPayload = person.getDataPayload();
            if (dataPayload == null) continue;
            JSONObject jsonData = null;
            try {
                jsonData = new JSONObject(dataPayload);
                long other_time = jsonData.getLong("when");
                if (now - other_time < difference)
                {
                    filtered.add(person);
                }
                else
                {
                    Log.e("SmartPeopleInfo", "Discarded" + person);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return filtered;
    }
}
