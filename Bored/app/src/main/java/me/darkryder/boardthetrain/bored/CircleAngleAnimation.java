package me.darkryder.boardthetrain.bored;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by darkryder on 4/2/16.
 */
public class CircleAngleAnimation extends Animation {

    private Circle circle;

    private float oldAngle;
    private float newAngle;

    public CircleAngleAnimation(Circle circle, int oldAngle, int newAngle) {
        this.oldAngle = oldAngle;
        circle.setAngle(oldAngle);
        this.newAngle = newAngle;
        this.circle = circle;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = oldAngle + ((newAngle - oldAngle) * interpolatedTime);

        circle.setAngle(angle);
        circle.requestLayout();
    }
}